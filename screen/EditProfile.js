import React, { useState } from "react";
import { View, StyleSheet, Text, useWindowDimensions } from "react-native";
import CustomInput from "../components/CustomInput/CustomInput";
// import CustomButton from "../../components/CustomButton";
import { useForm } from "react-hook-form";
import SelectPicker from "react-native-form-select-picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";

import { Overlay, Button, Avatar } from "react-native-elements";

// type OverlayComponentProps = {};

const EMAIL_REGEX =
  /^[a-zA-Z0-9.! #$%&'*+/=? ^_`{|}~-]+@[a-zA-Z0-9-]+(?:\. [a-zA-Z0-9-]+)*$/;
const options = ["Male", "Female"];
const EditProfile = ({ navigation }) => {
  const [visible, setVisible] = useState(false);
  // Hook
  const { control, handleSubmit } = useForm();
  const [selected, setSelected] = useState();
  // const { height } = useWindowDimensions();
  const onUpdatePress = () => {
    // Validate
    navigation.navigate("TabNavigation");
  };
  const onCancelPress = () => {
    // Validate
    navigation.navigate("TabNavigation");
  };
  const onConfirmPress = () => {
    setVisible(!visible);
  };

  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    console.warn("A date has been picked: ", date);
    hideDatePicker();
  };
  return (
    <View style={styles.root}>
      <Avatar
        size={64}
        title="BP"
        onPress={() => console.log("Works!")}
        activeOpacity={0.7}
        source={{
          uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6TaA5ehsE6l00c_BuFD1PrjhyedA_hT4b-g&usqp=CAU",
        }}
        containerStyle={{ margin: 10, alignSelf: "center" }}
      />
      <Text style={styles.text}>Full name</Text>
      <CustomInput
        name="fullName"
        control={control}
        placeholder="Full Name"
        secureTextEntry={false}
        rules={{ required: "Full name is required" }}
        paddingValue="2%"
      />

      <Text style={styles.text}>Username</Text>
      <CustomInput
        name="userName"
        control={control}
        placeholder="Username"
        secureTextEntry={false}
        rules={{ required: "Username is required" }}
        paddingValue="2%"
      />

      <Text style={styles.text}>Date of Birth</Text>
      <Button
        buttonStyle={{
          backgroundColor: "white",
          marginBottom: 10,
        }}
        title="Select Date of Birth"
        titleStyle={{ color: "black" }}
        onPress={showDatePicker}
      />

      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />

      <Text style={styles.text}>Gender</Text>
      <SelectPicker
        style={styles.select}
        placeholder="None"
        onValueChange={(value) => {
          setSelected(value);
        }}
        selected={selected}
      >
        {options.map((val, index) => {
          return <SelectPicker.Item label={val} value={val} key={val} />;
        })}
      </SelectPicker>

      <Text style={styles.text}>Phone Number</Text>
      <CustomInput
        name="phonenumber"
        control={control}
        placeholder="012xxx"
        secureTextEntry={false}
        paddingValue="2%"
      />

      <Text style={styles.text}>E-mail</Text>
      <CustomInput
        name="email"
        control={control}
        placeholder="Email"
        secureTextEntry={false}
        rules={{
          required: "Email is required",
          pattern: {
            value: EMAIL_REGEX,
            message: "Email is invalid",
          },
        }}
        paddingValue="2%"
      />

      <Text style={styles.text}>Password</Text>
      <CustomInput
        name="password"
        control={control}
        placeholder="Password"
        secureTextEntry={true}
        rules={{
          required: "Password is required",
          minLength: {
            value: 4,
            message: "Password should be minimum 4 characters long",
          },
        }}
        paddingValue="2%"
      />
      <View style={styles.setCenter}>
        <Overlay isVisible={visible} onBackdropPress={onConfirmPress}>
          <Text style={styles.textPrimary}>Successfully</Text>
          <Text style={styles.textSecondary}>
            Your profile has been updated
          </Text>
          <View style={styles.setCenter}>
            <Button
              title="Update"
              buttonStyle={{ backgroundColor: "black", marginBottom: 15 }}
              onPress={onUpdatePress}
            />
          </View>
        </Overlay>
      </View>
      <View style={styles.setCenter}>
        <Button
          title="Update"
          buttonStyle={{ backgroundColor: "black", marginBottom: 15 }}
          onPress={handleSubmit(onConfirmPress)}
          containerStyle={{ width: "80%", marginTop: 20 }}
        />
      </View>

      <View style={styles.setCenter}>
        <Button
          title="Cancel"
          buttonStyle={{ backgroundColor: "white", marginBottom: 10 }}
          titleStyle={{ color: "black" }}
          containerStyle={{ width: "80%" }}
          onPress={onCancelPress}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    padding: 20,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: "#F12084",
    flex: 1,
  },
  ava: {
    alignSelf: "center",
    width: "40%",
    height: "auto",
  },
  title: {
    fontSize: 32,
    fontWeight: "bold",
    textAlign: "left",
    color: "white",
  },
  line: {
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 0,
    paddingTop: 8,
  },
  textPrimary: {
    marginVertical: 20,
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 30,
  },
  textSecondary: {
    marginBottom: 10,
    textAlign: "center",
    fontSize: 17,
  },
  select: {
    backgroundColor: "white",
    width: "100%",
    borderColor: "#FFF8F8",
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 10,
    marginVertical: 15,
    padding: "2%",
  },
  text: {
    fontSize: 15,
    fontWeight: "bold",
    paddingRight: 85,
    textAlign: "left",
    color: "white",
  },
  setCenter: {
    alignItems: "center",
    // paddingTop: 30,
  },
});
export default EditProfile;
