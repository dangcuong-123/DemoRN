import { initializeApp } from "firebase/app";
import { getAuth } from "firebase/auth";

const firebaseConfig = {
  apiKey: "AIzaSyB5uckdw06yZkxkiSiQZlT-bBr3lh_DKTI",
  authDomain: "demorn-ae2fd.firebaseapp.com",
  projectId: "demorn-ae2fd",
  storageBucket: "demorn-ae2fd.appspot.com",
  messagingSenderId: "541112455288",
  appId: "1:541112455288:web:cc18743f2b62807e479d8d",
  measurementId: "G-KLW4TQRBN2",
};

const app = initializeApp(firebaseConfig);
const auth = getAuth(app);

export { auth };
