import { makeObservable, action, observable } from 'mobx'

interface account {
  email: string,
  password: string,
  fullname: string,
  username: string
}

class  ACCOUNT {
  // all account
  accounts: account[] = [
    {
      email: '1234',
      password: '1234',
      fullname: 'hieu',
      username: 'tomorrow'
    }
  ]

  account: account = {
    email: '',
    password: '',
    fullname: '',
    username: ''
  }
  currentAccount: account = {
    email: '',
    password: '',
    fullname: '',
    username: ''
  }

  constructor() {
    makeObservable(this, {
      accounts: observable,
      account: observable,
      currentAccount: observable,
      addAccount: action,
      update: action,
      checkLogin: action,
      changePassowrd: action,
      EditProfile: action,
      logout: action,
      getDataLogin: action
    })
  }

  public addAccount() {
    const email = this.getEmail()
    const password = this.getPassword()
    const fullname = this.getFullname()
    const username = this.getUsername()
    const index = this.checkEmailAccount(email)
    if (index < 0) {
      this.accounts = [...this.accounts, ...[{ email, password, fullname, username }]]
      return true
    }
    else {
      return false
    }
  }

  public update() {
    const email = this.getEmail()
    const password = this.getPassword()
    const index = this.checkEmailAccount(email)
    if (index >= 0) {
      this.accounts[index].password = password
      return true
    }
    else return false
  }

  public checkEmailAccount(email: string) {
    const index = this.accounts.findIndex(element => element.email == email)
    return index
  }

  public checkLogin() {
    const email = this.getEmail()
    const password = this.getPassword()
    const index = this.checkEmailAccount(email)
    const fullname = this.accounts[index]?.fullname
    const username = this.accounts[index]?.username
    if (index >= 0) {
      if (this.accounts[index].password == password) {
        this.currentAccount.email = email
        this.currentAccount.password = password
        this.currentAccount.fullname = fullname
        this.currentAccount.username = username
        return true
      }
      else return false
    }
    else return false
  }

  public getDataLogin() {
    return this.currentAccount
  }

  public changePassowrd(newPassword: string) {
    const index = this.checkEmailAccount(this.account?.email)
    console.log(index);
    this.accounts[index].password = newPassword
  }

  public EditProfile(fullname: string, username: string) {
    const index = this.checkEmailAccount(this.currentAccount?.email)
    this.accounts[index].fullname = fullname
    this.accounts[index].username = username
    this.currentAccount.fullname = fullname
    this.currentAccount.username = username
  }

  public logout() {
    this.currentAccount.email = ''
    this.currentAccount.password = ''
    this.currentAccount.fullname = ''
    this.account.email = ''
    this.account.username = ''
    this.account.fullname = ''
    this.account.password = ''

  }

  public setEmail(email: string) {
    this.account.email = email
  }

  public setPassword(password: string) {
    this.account.password = password
  }

  public setFullname(fullname: string) {
    this.account.fullname = fullname
  }

  public setUsername(username: string) {
    this.account.username = username
  }

  public getEmail() {
    return this.account.email
  }

  public getPassword() {
    return this.account.password
  }

  public getFullname() {
    return this.account.fullname
  }

  public getUsername() {
    return this.account.username
  }
}

const Account = new ACCOUNT()
export default Account