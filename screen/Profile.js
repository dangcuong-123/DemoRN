import React from "react";
import {
  View,
  StyleSheet,
  Text,
  Image,
  useWindowDimensions,
} from "react-native";
// import CustomButton from "../../components/CustomButton";
import { useForm } from "react-hook-form";
import { Icon, Avatar, Button } from "react-native-elements";

const ProfileScreen = ({ navigation }) => {
  //   const ava = require("../../../assets/ava.png");

  const { height } = useWindowDimensions();

  // Hook
  const { control, handleSubmit, watch } = useForm();
  const pwd = watch("password");

  const onLogOutPress = () => {
    // Validate
    navigation.navigate("Login");
  };
  const onEditPress = () => {
    // Validate
    navigation.navigate("EditProfile");
  };

  return (
    <View style={styles.root}>
      <Avatar
        size={64}
        title="BP"
        onPress={() => console.log("Works!")}
        activeOpacity={0.7}
        source={{
          uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6TaA5ehsE6l00c_BuFD1PrjhyedA_hT4b-g&usqp=CAU",
        }}
        containerStyle={{ margin: 10, alignSelf: "center" }}
      />
      {/* Information */}
      <View style={styles.in_line_other}>
        <Text style={styles.name}>Jenifer</Text>
        <Icon
          name={"edit"}
          containerStyle={styles.icon}
          onPress={onEditPress}
          tvParallaxProperties={undefined}
        />
      </View>

      <View style={{ marginTop: "10%" }} />
      <Text style={styles.title}>Basic Inf</Text>
      <View style={styles.line}>
        <View style={{ flex: 1, height: 2, backgroundColor: "white" }} />
      </View>
      <View style={styles.in_line}>
        <Text style={styles.text}>Full name</Text>
        <Text style={styles.text}>Jenifer</Text>
      </View>
      <View style={styles.in_line}>
        <Text style={styles.text}>Birthday</Text>
        <Text style={styles.text}>28.01.2000</Text>
      </View>
      <View style={styles.in_line}>
        <Text style={styles.text}>Gender</Text>
        <Text style={styles.text}>Rather not say</Text>
      </View>
      <View style={styles.in_line}>
        <Text style={styles.text}>Password</Text>
        <Text style={styles.text}>********</Text>
      </View>

      {/* Contact */}
      <View style={{ marginTop: "10%" }} />
      <Text style={styles.title}>Contact Inf</Text>
      <View style={styles.line}>
        <View style={{ flex: 1, height: 2, backgroundColor: "white" }} />
      </View>
      <View style={styles.in_line}>
        <Text style={styles.text}>Email</Text>
        <Text style={styles.text}>Jenifer</Text>
      </View>
      <View style={styles.in_line}>
        <Text style={styles.text}>Phone</Text>
        <Text style={styles.text}>123456789</Text>
      </View>
      <View style={styles.setCenter}>
        <Button
          title="Log Out"
          buttonStyle={{ backgroundColor: "black", marginBottom: 10 }}
          onPress={handleSubmit(onLogOutPress)}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  root: {
    padding: 20,
    paddingLeft: 25,
    paddingRight: 25,
    backgroundColor: "#F12084",
    flex: 1,
  },
  ava: {
    alignSelf: "center",
    width: "40%",
    height: "auto",
  },
  name: {
    alignSelf: "center",
    fontSize: 30,
    display: "flex",
    color: "white",
  },
  icon: {
    backgroundColor: "#F12084",
  },
  title: {
    fontSize: 26,
    // fontWeight: "bold",
    textAlign: "left",
    color: "white",
  },
  line: {
    flexDirection: "row",
    alignItems: "center",
    borderWidth: 0,
    paddingTop: 8,
  },
  in_line: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  in_line_other: {
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  text: {
    paddingTop: 20,
    fontSize: 17,
    // paddingRight: 85,
    textAlign: "left",
    color: "white",
  },
  setCenter: {
    alignItems: "center",
    paddingTop: 30,
  },
});
export default ProfileScreen;
