import React from "react";
import { NavigationContainer, DefaultTheme } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Login from "./screen/Login";
import ForgotPassword from "./screen/ForgotPassword";
import Register from "./screen/Register";
import NewPassword from "./screen/NewPassword";
import Home from "./screen/Home";
import TabNavigation from "./components/TabNavigation";
import EditProfile from "./screen/EditProfile";
import Stream from "./screen/Stream";

const Stack = createNativeStackNavigator();
const AppTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: "#F12084",
  },
};

export default class App extends React.Component {
  render() {
    return (
      <NavigationContainer theme={AppTheme}>
        <Stack.Navigator
          initialRouteName="Login"
          screenOptions={{ headerShown: false }}
        >
          <Stack.Screen name="Login" component={Login} />
          <Stack.Screen name="ForgotPassword" component={ForgotPassword} />
          <Stack.Screen name="Register" component={Register} />
          <Stack.Screen name="NewPassword" component={NewPassword} />
          <Stack.Screen name="Home" component={Home} />
          <Stack.Screen name="TabNavigation" component={TabNavigation} />
          <Stack.Screen name="EditProfile" component={EditProfile} />
          <Stack.Screen name="Stream" component={Stream} />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
