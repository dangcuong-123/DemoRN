import React from "react";
import { View, Text, StyleSheet } from "react-native";
import { Avatar, SearchBar } from "react-native-elements";
import { useState } from "react";
import Swiper from "react-native-swiper/src";
import ListHome from "../components/ListHome/ListHome";

export default function Home() {
  const [search, setSearch] = useState("");

  const updateSearch = (search) => {
    setSearch(search);
  };

  return (
    <View style={{ backgroundColor: "#F12084", padding: 10 }}>
      <View style={{ flexDirection: "row" }}>
        <Avatar
          size="medium"
          title="BP"
          onPress={() => console.log("Works!")}
          activeOpacity={0.7}
          source={{
            uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6TaA5ehsE6l00c_BuFD1PrjhyedA_hT4b-g&usqp=CAU",
          }}
          containerStyle={{ margin: 10 }}
        />

        <Text
          style={{
            fontSize: 25,
            color: "white",
            alignSelf: "center",
            marginLeft: 15,
            fontWeight: "bold",
          }}
        >
          Hi, Jennifer!
        </Text>
      </View>

      <SearchBar
        showCancel
        lightTheme
        // showLoading
        round
        containerStyle={{ backgroundColor: "#F12084", borderColor: "#F12084" }}
        inputContainerStyle={{ backgroundColor: "rgba(16, 15, 15, 0.6)" }}
        inputStyle={{ color: "white" }}
        placeholderTextColor="black"
        onChangeText={updateSearch}
        platform={"default"}
        value={search}
      />
      <View style={{ margin: 10 }}>
        <Swiper style={styles.wrapper} showsButtons={true}>
          <View style={styles.slide1}>
            <Text style={styles.text}>Living</Text>
          </View>
          <View style={styles.slide2}>
            <Text style={styles.text}>Popular</Text>
          </View>
          <View style={styles.slide3}>
            <Text style={styles.text}>Community</Text>
          </View>
        </Swiper>
      </View>

      <ListHome
        title="How’s your day?"
        singer="Lawrence Singer"
        seen="2000"
        like="2500"
        cost="6500"
      ></ListHome>
      <ListHome
        title="Let’ sing together"
        singer="Free"
        seen="2000"
        like="2500"
        cost="6500"
      ></ListHome>
      <ListHome
        title="Hmmm....."
        singer="Mark"
        seen="2000"
        like="2500"
        cost="6500"
      ></ListHome>
    </View>
  );
}

const styles = StyleSheet.create({
  wrapper: { height: 200, margin: 10 },
  slide1: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#9DD6EB",
  },
  slide2: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#97CAE5",
  },
  slide3: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#92BBD9",
  },
  text: {
    color: "#fff",
    fontSize: 30,
    fontWeight: "bold",
  },
});
