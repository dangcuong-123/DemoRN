import React, { useState } from "react";
import { View, StyleSheet, useWindowDimensions } from "react-native";
import { Input, Text, Button, Icon, Overlay } from "react-native-elements";
import { useForm, Controller } from "react-hook-form";
import Account from "../store/Account";

export default function NewPassword({ route, navigation }) {
  const { height } = useWindowDimensions();
  const { email } = route.params;
  const {
    control,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const newPassword = watch("newPassword");
  const [visible, setVisible] = useState(false);

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const checkChangePassword = (data) => {
    Account.changePassowrd(data.newPassword);
    toggleOverlay();
  };

  const loginNow = () => {
    toggleOverlay();
    navigation.navigate("Login");
  };
  const backScreen = () => navigation.navigate("ForgotPassword");

  return (
    <View style={{ paddingHorizontal: "5%", marginTop: 30 }}>
      <View
        style={{
          flexDirection: "row",
          paddingVertical: 8,
          borderBottomWidth: 1,
          borderColor: "white",
          marginBottom: 20,
        }}
      >
        <Icon
          style={{ marginBottom: 0, paddingBottom: 0 }}
          name="chevron-left"
          size={33}
          color="white"
          onPress={backScreen}
        ></Icon>

        <Text
          style={{ fontSize: 30, color: "white", fontWeight: "bold" }}
          onPress={backScreen}
        >
          New Password
        </Text>
      </View>
      <View style={{ marginTop: height * 0.2 }} />

      <Text style={styles.label}>New Password</Text>
      <Controller
        control={control}
        rules={{
          required: "This is required",
          minLength: {
            value: 4,
            message: "Password should be minimum 4 characters long",
          },
        }}
        render={({
          field: { onChange, onBlur, value },
          fieldState: { error },
        }) => (
          <>
            <Input
              placeholder="Enter password"
              type="password"
              onChangeText={onChange}
              onBlur={onBlur}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              containerStyle={styles.input}
              secureTextEntry={true}
            />
            {error && (
              <Text style={styles.errors}>
                {error.message || "This is required"}
              </Text>
            )}
          </>
        )}
        name="newPassword"
      />

      <Text style={styles.label}>Confirm Password</Text>
      <Controller
        control={control}
        rules={{
          required: "This is required",
          minLength: {
            value: 4,
            message: "Password should be minimum 4 characters long",
          },
          validate: (value) => value === newPassword || "Password do not match",
        }}
        render={({
          field: { onChange, onBlur, value },
          fieldState: { error },
        }) => (
          <>
            <Input
              placeholder="Enter Confirm Password"
              type="password"
              onChangeText={onChange}
              onBlur={onBlur}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              containerStyle={styles.input}
              secureTextEntry={true}
            />
            {error && (
              <Text style={styles.errors}>
                {error.message || "This is required"}
              </Text>
            )}
          </>
        )}
        name="confirmPassword"
      />
      <Button
        title="Confirm"
        buttonStyle={{
          backgroundColor: "black",
          marginHorizontal: 20,
          marginVertical: 30,
        }}
        onPress={handleSubmit(checkChangePassword)}
      />
      <Overlay
        overlayStyle={{
          flex: 1,
          textAlign: "center",
          alignItems: "center",
          justifyContent: "center",
          maxHeight: 200,
        }}
        isVisible={visible}
        onBackdropPress={toggleOverlay}
      >
        <View>
          <Text style={styles.notificationTitle}>Successfully!</Text>
          <Text style={styles.notificationText}>
            Your password has been changed Successfully
          </Text>
          <Button
            buttonStyle={{
              backgroundColor: "black",
              marginHorizontal: 10,
              marginVertical: 10,
            }}
            title="Login now"
            onPress={loginNow}
          />
        </View>
      </Overlay>
    </View>
  );
}

const styles = StyleSheet.create({
  errors: {
    marginBottom: 10,
    color: "black",
  },
  label: {
    fontSize: 15,
    color: "white",
    fontWeight: "bold",
    marginBottom: 10,
  },
  input: {
    backgroundColor: "white",
    borderRadius: 5,
    marginBottom: 17,
    height: 40,
  },
});
