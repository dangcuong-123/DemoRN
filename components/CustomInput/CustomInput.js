import React from "react";
import { Controller } from "react-hook-form";
import { Text, View, TextInput, StyleSheet } from "react-native";

const CustomInput = ({
  control,
  name,
  rules = {},
  placeholder,
  secureTextEntry,
  paddingValue,
}) => {
  return (
    <Controller
      control={control}
      name={name}
      rules={rules}
      render={({
        field: { value, onChange, onBlur },
        fieldState: { error },
      }) => (
        <>
          <View
            style={[
              styles.container,
              {
                padding: paddingValue ? paddingValue : 20,
                borderColor: error ? "black" : "#FFF8F8",
              },
            ]}
          >
            <TextInput
              placeholder={placeholder}
              onChangeText={onChange}
              onBlur={onBlur}
              secureTextEntry={secureTextEntry}
            />
          </View>
          {error && (
            <Text style={{ color: "black", alignSelf: "stretch" }}>
              {error.message || "Must enter information!!!"}
            </Text>
          )}
        </>
      )}
    />
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    width: "100%",
    borderColor: "#FFF8F8",
    borderWidth: 1,
    borderRadius: 10,
    paddingHorizontal: 10,
    marginVertical: 15,
  },
  input: {},
});

export default CustomInput;
