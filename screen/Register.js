import React, { useState } from "react";
import { View, StyleSheet, useWindowDimensions } from "react-native";
import { Input, Text, Button, Icon, Overlay } from "react-native-elements";
import { useForm, Controller } from "react-hook-form";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Account from "../store/Account";

export default function Register({ navigation }) {
  const {
    control,
    handleSubmit,
    watch,
    formState: { errors },
  } = useForm();
  const password = watch("password");
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [visible, setVisible] = useState(false);
  const [dates, setDate] = useState("Select date of birth");

  const toggleOverlay = () => {
    setVisible(!visible);
  };

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    setDate(date.getFullYear() + "/" + date.getMonth() + "/" + date.getDate());
    hideDatePicker();
  };
  const loginNow = () => {
    navigation.navigate("Login");
  };

  const register = (data) => {
    const { email, fullname, password } = data;
    Account.setEmail(email);
    Account.setFullname(fullname);
    Account.setPassword(password);

    if (Account.addAccount()) {
      toggleOverlay();
    } else {
      console.warn("email already exists");
    }
  };

  const backScreen = () => navigation.navigate("Login");

  return (
    <View style={{ paddingHorizontal: "5%", marginTop: 30 }}>
      <View
        style={{
          flexDirection: "row",
          paddingVertical: 8,
          borderBottomWidth: 1,
          borderColor: "white",
          marginBottom: 20,
        }}
      >
        <Icon
          style={{ marginBottom: 0, paddingBottom: 0 }}
          name="chevron-left"
          size={33}
          color="white"
          onPress={backScreen}
        ></Icon>
        <Text
          style={{ fontSize: 30, color: "white", fontWeight: "bold" }}
          onPress={backScreen}
        >
          Register
        </Text>
      </View>

      <Text style={styles.label}>Full name*</Text>

      <Controller
        control={control}
        rules={{
          required: true,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <Input
            placeholder="Full Name"
            onChangeText={onChange}
            onBlur={onBlur}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            containerStyle={styles.input}
            // secureTextEntry={true}
          />
        )}
        name="fullname"
      />
      {errors.fullname && <Text style={styles.errors}>This is required.</Text>}

      <Text style={styles.label}>E-mail*</Text>

      <Controller
        control={control}
        rules={{
          required: true,
        }}
        render={({ field: { onChange, onBlur, value } }) => (
          <Input
            placeholder="E-mail"
            onChangeText={onChange}
            onBlur={onBlur}
            inputContainerStyle={{ borderBottomWidth: 0 }}
            containerStyle={styles.input}
          />
        )}
        name="email"
      />
      {errors.email && <Text style={styles.errors}>This is required.</Text>}

      <Text style={styles.label}>Date of Birth</Text>
      <Button
        buttonStyle={{
          backgroundColor: "white",
          marginBottom: 10,
        }}
        title={dates}
        titleStyle={{ color: "black" }}
        onPress={showDatePicker}
      />
      <DateTimePickerModal
        isVisible={isDatePickerVisible}
        mode="date"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />

      <Text style={styles.label}>Password*</Text>

      <Controller
        control={control}
        rules={{
          required: "This is required",
          minLength: {
            value: 4,
            message: "Password should be minimum 4 characters long",
          },
        }}
        render={({
          field: { onChange, onBlur, value },
          fieldState: { error },
        }) => (
          <>
            <Input
              placeholder="Password"
              onChangeText={onChange}
              onBlur={onBlur}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              containerStyle={styles.input}
              secureTextEntry={true}
            />
            {error && (
              <Text style={styles.errors}>
                {error.message || "This is required"}
              </Text>
            )}
          </>
        )}
        name="password"
      />

      {/* {errors.password && <Text style={styles.errors}>This is required.</Text>}
      <Text style={styles.label}>Confirm Password*</Text> */}

      <Controller
        control={control}
        rules={{
          required: true,
          minLength: {
            value: 4,
            message: "Password should be minimum 4 characters long",
          },
          validate: (value) => value === password || "Password do not match",
        }}
        render={({
          field: { onChange, onBlur, value },
          fieldState: { error },
        }) => (
          <>
            <Input
              placeholder="Confirm Password"
              onChangeText={onChange}
              onBlur={onBlur}
              inputContainerStyle={{ borderBottomWidth: 0 }}
              containerStyle={styles.input}
              secureTextEntry={true}
            />
            {error && (
              <Text style={styles.errors}>
                {error.message || "This is required"}
              </Text>
            )}
          </>
        )}
        name="confirmPassword"
      />
      {/* {errors.confirmPassword && (
        <Text style={styles.errors}>This is required.</Text>
      )} */}
      <Button
        title="Register"
        buttonStyle={styles.buttonStyle}
        onPress={handleSubmit(register)}
      />
      <Overlay
        overlayStyle={{
          flex: 1,
          textAlign: "center",
          alignItems: "center",
          justifyContent: "center",
          maxHeight: 200,
        }}
        isVisible={visible}
        onBackdropPress={toggleOverlay}
      >
        <View>
          <Text style={styles.notificationTitle}>Successfully!</Text>
          <Text style={styles.notificationText}>
            Your password has been changed Successfully
          </Text>
          <Button
            buttonStyle={{
              backgroundColor: "black",
              marginHorizontal: 10,
              marginVertical: 10,
            }}
            title="Login now"
            onPress={loginNow}
          />
        </View>
      </Overlay>
    </View>
  );
}

const styles = StyleSheet.create({
  buttonStyle: {
    backgroundColor: "black",
    marginHorizontal: 20,
    marginVertical: 30,
  },
  errors: {
    marginBottom: 10,
    color: "black",
  },
  label: {
    color: "white",
    fontSize: 20,
    fontWeight: "bold",
    marginBottom: 10,
  },
  notificationTitle: {
    fontSize: 20,
    fontWeight: "bold",
  },
  notificationText: {
    fontSize: 15,
  },
  input: {
    backgroundColor: "white",
    borderRadius: 5,
    marginBottom: 17,
    height: 40,
  },
});
