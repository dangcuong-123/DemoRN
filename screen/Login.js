import React, { useState } from "react";
import { View, StyleSheet } from "react-native";
import IconLiveStream from "../components/Icons/IconLiveStream";
import { Input, Text, Button, SocialIcon } from "react-native-elements";
import { useForm, Controller } from "react-hook-form";
import Account from "../store/Account";
// import { auth } from "../libs/ConfigFirebase.js";
// import {
//   signInWithEmailAndPassword,
//   signInWithPopup,
//   // GoogleAuthProvider,
// } from "firebase/auth";

export default function Login({ navigation }) {
  const {
    control,
    handleSubmit,
    formState: { errors },
  } = useForm();
  // const [authError, setAuthError] = useState("");
  const onSubmit = (data) => {
    let { email, password } = data;
    Account.setEmail(email);
    Account.setPassword(password);
    if (Account.checkLogin()) {
      navigation.navigate("TabNavigation");
    } else {
      console.warn("account not in db");
    }
  };

  // const provider = new GoogleAuthProvider();
  const handleGoogleLogin = () => {};
  //   signInWithPopup(auth, provider)
  //     .then((result) => {
  //       // This gives you a Google Access Token. You can use it to access the Google API.
  //       const credential = GoogleAuthProvider.credentialFromResult(result);
  //       const token = credential.accessToken;
  //       // The signed-in user info.
  //       const user = result.user;
  //       // ...
  //       console.log(result);
  //     })
  //     .catch((error) => {
  //       // Handle Errors here.
  //       const errorCode = error.code;
  //       const errorMessage = error.message;
  //       // The email of the user's account used.
  //       const email = error.customData.email;
  //       // The AuthCredential type that was used.
  //       const credential = GoogleAuthProvider.credentialFromError(error);
  //       // ...
  //     });
  // };

  return (
    <View style={styles.root}>
      <View>
        <View
          style={{
            justifyContent: "center",
            alignItems: "center",
            marginBottom: 20,
          }}
        >
          <IconLiveStream />
        </View>

        <View style={styles.backGroundInput}>
          <Text h1 style={{ marginBottom: 20 }}>
            Welcome!
          </Text>
          <Controller
            control={control}
            rules={{
              required: true,
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <Input
                placeholder="Enter email ID"
                type="email"
                onChangeText={onChange}
                onBlur={onBlur}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={styles.input}
              />
            )}
            name="email"
          />

          <Controller
            control={control}
            rules={{
              required: true,
              minLength: {
                value: 4,
                message: "Password should be minimum 4 characters long",
              },
            }}
            render={({ field: { onChange, onBlur, value } }) => (
              <Input
                placeholder="Enter password"
                type="password"
                onChangeText={onChange}
                onBlur={onBlur}
                inputContainerStyle={{ borderBottomWidth: 0 }}
                containerStyle={styles.input}
                rightIcon={{
                  type: "material-community",
                  name: "eye-off-outline",
                  iconStyle: { color: "gray", backgroundColor: "white" },
                  size: 20,
                }}
                secureTextEntry={true}
              />
            )}
            name="password"
          />
          {(errors.email || errors.password) && (
            <Text
              style={{
                marginBottom: 20,
                color: "#FFFBFB",
              }}
            >
              Email or password wrong. Please try again.
            </Text>
          )}
          <Text
            style={{
              color: "white",
              marginBottom: 20,
              textAlign: "right",
              fontSize: 15,
              textDecorationLine: "underline",
            }}
            onPress={() => navigation.navigate("ForgotPassword")}
          >
            Forgot password?
          </Text>
          <Button
            title="Log in"
            buttonStyle={{ backgroundColor: "black", marginBottom: 10 }}
            onPress={handleSubmit(onSubmit)}
          />
          <View
            style={{
              flexDirection: "row",
              justifyContent: "center",
              alignContent: "center",
            }}
          >
            <SocialIcon type="google" onPress={handleGoogleLogin} />
            <SocialIcon type="facebook" />
            <SocialIcon type="twitter" />
          </View>
        </View>
        <View
          style={{
            flex: 1,
            justifyContent: "center",
            alignItems: "center",
            margin: 20,
          }}
        >
          <Text style={{ color: "white", fontSize: 15 }}>
            Don’t have an account ?
            <Text
              style={{
                color: "white",
                fontSize: 15,
                textDecorationLine: "underline",
              }}
              onPress={() => navigation.navigate("Register")}
            >
              Create account
            </Text>
          </Text>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  root: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
  },
  backGroundInput: {
    backgroundColor: "#BF1A69",
    padding: 20,
    borderRadius: 20,
    marginBottom: 15,
    shadowOffset: {
      width: 0,
      height: 4,
    },
    shadowOpacity: 0.58,
    shadowRadius: 5.46,
    elevation: 24,
  },
  input: {
    backgroundColor: "white",
    borderRadius: 5,
    marginBottom: 17,
    height: 40,
  },
});
