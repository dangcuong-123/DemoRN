import React, { Component } from "react";
import { Text, StyleSheet, View } from "react-native";
import { Avatar, Icon } from "react-native-elements";

export default function ListHome(props) {
  return (
    <View
      style={{
        justifyContent: "center",
        alignItems: "center",
        marginTop: 10,
      }}
    >
      <View
        style={{
          backgroundColor: "white",
          padding: 10,
          borderRadius: 10,
          width: 370,
          flexDirection: "row",
        }}
      >
        <Avatar
          size="large"
          title="BP"
          onPress={() => console.log("Works!")}
          activeOpacity={0.7}
          source={{
            uri: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR6TaA5ehsE6l00c_BuFD1PrjhyedA_hT4b-g&usqp=CAU",
          }}
          avatarStyle={{ flex: 1 }}
        />
        <View style={{ marginLeft: 20, flex: 2 }}>
          <Text style={{ fontSize: 20, color: "green" }}>{props.title}</Text>
          <Text> {props.singer} </Text>
          <View style={{ flexDirection: "row", marginTop: 10 }}>
            <View style={styles.icons}>
              <Icon name="visibility" color="#099929" />
              <Text style={{ alignSelf: "center" }}>{props.seen}</Text>
            </View>

            <View style={styles.icons}>
              <Icon name="favorite" color="#F21E1E" />
              <Text style={{ alignSelf: "center" }}>{props.like}</Text>
            </View>

            <View style={styles.icons}>
              <Icon
                type="material-community"
                name="currency-usd"
                color="#00A0FF"
              />
              <Text style={{ alignSelf: "center" }}>{props.cost}</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  icons: {
    flexDirection: "row",
    flex: 1,
  },
});
