import React, { useState } from "react";
import { View, StyleSheet, useWindowDimensions, Alert } from "react-native";
import { Input, Text, Button, Icon } from "react-native-elements";
import emailjs from "@emailjs/browser";
import Account from "../store/Account";

const YOUR_SERVICE_ID = "service_bn2dtiq";
const YOUR_TEMPLATE_ID = "template_l9j75fi";
const YOUR_PUBLIC_KEY = "YZLGS7fRXjZzhs_wO";

export default function ForgotPassword({ navigation }) {
  const { height } = useWindowDimensions();
  const [email, setEmail] = useState("");

  const backScreen = () => navigation.navigate("Login");
  const onSubmit = () => {
    var params = {
      toEmail: email,
      message: Math.floor(100000 + Math.random() * 900000),
    };
    if (Account.checkEmailAccount(email) >= 0) {
      // emailjs
      //   .send(YOUR_SERVICE_ID, YOUR_TEMPLATE_ID, params, YOUR_PUBLIC_KEY)
      //   .then(
      //     function (response) {
      //       Alert.alert("SENT EMAIL SUCCESS!");
      //       navigation.navigate("NewPassword");
      //       console.log("SUCCESS!", response.status, response.text);
      //     },
      //     function (error) {
      //       Alert.alert("SENT EMAIL FAILED...!");
      //       console.log("FAILED...", error);
      //     }
      //   );
      Account.setEmail(email);
      navigation.navigate("NewPassword", { email: email });
    } else {
      Alert.alert("Email not found!");
      console.log("Email not found!");
    }
  };

  return (
    <View style={{ paddingHorizontal: "5%", marginTop: 30 }}>
      <View
        style={{
          flexDirection: "row",
          paddingVertical: 8,
          borderBottomWidth: 1,
          borderColor: "white",
          marginBottom: 20,
        }}
      >
        <Icon
          style={{ marginBottom: 0, paddingBottom: 0 }}
          name="chevron-left"
          size={33}
          color="white"
          onPress={backScreen}
        ></Icon>

        <Text
          style={{ fontSize: 30, color: "white", fontWeight: "bold" }}
          onPress={backScreen}
        >
          Forgot Password
        </Text>
      </View>
      <View style={{ marginTop: height * 0.2 }} />

      <Text
        style={{
          fontSize: 15,
          color: "white",
          fontWeight: "bold",
          marginBottom: 10,
          textAlign: "center",
        }}
      >
        Enter your email address and we will send a link to reset your password
      </Text>
      <Input
        placeholder="Enter your email"
        inputContainerStyle={{ borderBottomWidth: 0 }}
        containerStyle={styles.input}
        onChangeText={(text) => setEmail(text)}
        value={email}
      />

      <Button
        title="Send Email"
        buttonStyle={{
          backgroundColor: "black",
          marginHorizontal: 20,
          marginVertical: 30,
        }}
        onPress={onSubmit}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  input: {
    backgroundColor: "white",
    borderRadius: 5,
    marginBottom: 17,
    height: 40,
  },
});
